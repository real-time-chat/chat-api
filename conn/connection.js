const express = require('express')
const bodyParser = require('body-parser')
const mongoose = require('mongoose')
const routes = require('../routes/chatRouter')
const app = express()
const cors = require('cors');
app.use(cors());

const config = require('./db')
const PORT = 3000
const PORT2 = 3001

/**
 * Connection to Mongo DB
 */
mongoose.connect(config.DB, {useNewUrlParser: true}).then(
    () => {
        console.log('Successful connection')
    },
    error => {
        console.log(`Connection error ${error}`)
    })

app.use(bodyParser.json())
// Import defined routes for the api
app.use('/api', routes)
app.use((err, req, res, next) => {
    res.status(422).send({error: err.message}) //Syntax error
})

//Listening Express app port
app.listen(PORT, () => {
    console.log(`Running in port: ${PORT}`)
})

//Configuring http server for Socket.io
const http = require('http');
const server = http.createServer(app);
const socket_io = require('socket.io')
const io = socket_io(server)
//Importing services which interact with Mongo DB
const {userJoin, formatMessage, getRoomUsers, userLeave, getMessages} = require("../services");

const botName = 'ChatRoom Bot';

/**
 * Run when client app connects to the socket
 */
io.on('connection', socket => {
    /**
     * Executes when client app joins to the socket room
     */
    socket.on('joinRoom', ({id, username, room, moderator}) => {
        socket.join(room);
        // Register chat user into de DB
        let usrJoin = userJoin(id, username, room, moderator);
        usrJoin.then(sessions=>{
            //Obtain all chat room online users
            let getUsers = getRoomUsers(room)
            getUsers.then(users => {
                // Send users and room info
                io.to(room).emit('roomUsers', users);
            }).catch(error => {
                console.log(error)
            })
            //Check if the user has multiple opened sessions
            if(sessions===1){
                // Broadcast when a user connects
                socket.broadcast
                    .to(room)
                    .emit(
                        'message',
                        formatMessage(botName, `${username} has joined to the chat room`)
                    );
            }
        }).catch(error => {
            console.log(error)
        })

        // Obtain all the messages stored in the database
        let getMsg = getMessages()
        getMsg.then(messages => {
            if (messages.length > 0) {
                //Emit the messages to the client app
                socket.emit('message', messages);
            }
            // Welcome current user
            socket.emit('message', formatMessage(botName, 'Welcome to the chat room!'));
        }).catch(error => {
            console.log(error)
        })
    });
    // Listen for chatMessage
    socket.on('chatMessage', ({username, room, msg, moderator}) => {
        //Emit a message to all client apps
        io.to(room).emit('message', formatMessage(username, msg, moderator));
    });

    // Runs when client disconnects
    socket.on('disconnectU', ({id, username, room}) => {
        // Log out a chat user from the database
        let usrLeave = userLeave(id);
        usrLeave.then(sessions => {
            //Check if the user has multiple opened sessions
            if(sessions===1){
                // Broadcast when a user disconnects
                socket.broadcast
                    .to(room)
                    .emit(
                        'message',
                        formatMessage(botName, `${username} has left the session`)
                    );
                //Obtain all chat room online users
                let getUsers = getRoomUsers(room)
                getUsers.then(users => {
                    // Send users and room info
                    io.to(room).emit('roomUsers', users);
                }).catch(error => {
                    console.log(error)
                })
            }
        }).catch(error => {
            console.log(error)
        })

    });
});

//Listening Socket.io port
server.listen(PORT2, () => console.log(`Server running on port ${PORT2}`));
