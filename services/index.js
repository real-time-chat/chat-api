'use strict';

const jwt = require('jwt-simple');
const moment = require('moment');
const config = require('../config');
const chatModel=require('../models/chatModel')
const chatUserModel=require('../models/chatUserModel')

/**
 * Creates the Json Web Token
 * @param user User information
 * @returns {string} Web Token
 */
function createToken (user) {
    const code = {
        sub: user.email,
        iat: moment().unix(),
        exp: moment().add(14, 'days').unix()
    }
    return jwt.encode(code, config.HIDDEN_KEY)
}

/**
 * Decodes the JWT and determines if keeps valind or not
 * @param token
 * @returns {Promise<unknown>}
 */
function decodeToken (token) {
    return new Promise((resolve, reject) => {
        try {
            const message = jwt.decode(token, config.HIDDEN_KEY);

            if (message.exp <= moment().unix()) {
                reject({
                    status: 401,
                    message: 'Token has expired'
                })
            }
            resolve(message.sub)
        } catch (err) {
            reject({
                status: 500,
                message: 'Invalid token'
            })
        }
    })
}

/**
 * Register a chat user into the DB
 * @param id
 * @param username
 * @param room
 * @param moderator
 * @returns {Promise<unknown>}
 */
function userJoin(id, username, room,moderator) {
    return new Promise((resolve,reject)=>{
        const newChatUser= new chatUserModel({
            _id: id,
            username: username,
            room: room,
            sessions:1,
            moderator: moderator
        })

        newChatUser.save(err =>{
            if(err){
                // Checks the chat user sessions in DB and increases in case is already logged in
                chatUserModel.findByIdAndUpdate(id,{$inc:{sessions:1}},err=>{
                    if(err){
                        reject(err)
                    }
                    resolve(2)
                })
            } else {
                resolve(1)
            }
        })
    })
}

/**
 * Log out a chat user from the database
 * @param id
 * @returns {Promise<unknown>}
 */
function userLeave(id) {
    return new Promise((resolve,reject)=>{
        chatUserModel.findOneAndDelete({_id:id, sessions:1}, (err,user)=>{
            if(user){
                resolve(user.sessions)
            } else {
                chatUserModel.findByIdAndUpdate(id,{$inc:{sessions:-1}},err=>{
                    if(err){
                        reject(err)
                    } else {
                        resolve(2)
                    }
                })
            }
        })
    })
}

/**
 * Gets all the chat users logged in the database
 * @param room
 * @returns {Promise<unknown>}
 */
function getRoomUsers(room) {
    return new Promise((resolve,reject)=>{
        chatUserModel.find({'room':room},(err,users)=>{
            if(err){
                reject(err)
            }
            resolve(users)
        })
    })
}

/**
 * Obtains all the massages of the chat which are stored in the
 * @returns {Promise<unknown>}
 */
function getMessages() {
    return new Promise((resolve,reject)=>{
        chatModel.find({},(err,messages)=>{
            if(err){
                reject(err)
            }
            resolve(messages)
        })
    })
}

/**
 * Receives a message from the socket and formats it to be stored in the database
 * @param username
 * @param text
 * @param moderator
 * @returns {{moderator: boolean, text: *, time: string, username: *}[]|{moderator: boolean, text: *, time: string, username: *}}
 */
function formatMessage(username, text, moderator=false) {
    let time=moment().format('h:mm a')
    if(username==='ChatRoom Bot'){
        return {username, text, time: time, moderator};
    } else {
        const newChat= new chatModel({
            username:username,
            text:text,
            time:time,
            moderator:moderator
        })

        newChat.save(err=>{
            if(err){
                console.log('Error creating a new message: ' + err)
            }
        })

        return [{_id:newChat._id,username, text, time: time,moderator}];
    }
}

module.exports = {
    createToken: createToken,
    decodeToken: decodeToken,
    userJoin: userJoin,
    userLeave:userLeave,
    getRoomUsers:getRoomUsers,
    formatMessage:formatMessage,
    getMessages:getMessages,
}
