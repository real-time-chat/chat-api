'use strict'

const userModel = require('../models/userModel')
const services = require('../services');
const bcrypt=require('bcrypt')

/**
 * Receives data from the incoming http request and registers a user into the database
 * @param req Incoming user information
 * @param res Server response
 */
function register (req, res) {
    const newUser = new userModel({
        name: req.body.name,
        surname: req.body.surname,
        email: req.body.email,
        moderator:req.body.moderator,
        password: req.body.password,
    })

    //Saves the user as a MongoDB Document
    newUser.save((err) => {
        if (err) return res.status(500).send({ message: `Error while creating a new user: ${err}` })
        return res.status(201).send({ message: 'User is successfully created' })
    })
}

/**
 * Receives data from the incoming http request and Logs in
 * @param req Incoming user information
 * @param res Server request
 */
function login (req, res) {
    userModel.findOne({ email: req.body.email}, (err, user) => {
        if (err) return res.status(500).send({ message: err })
        if (!user) return res.status(404).send({ message: 'User does not exist' })
        bcrypt.compare(req.body.password, user.password, function(err, result) {
            if(result){
                req.user = user
                return res.status(200).send({
                    msg: 'You are logged in',
                    token: services.createToken(user) ,
                    user_id: user._id,
                    user_name:user.name + ' ' + user.surname,
                    moderator:user.moderator})
            } else {
                return res.status(404).send({ msg: `Password error: ${req.body.email}` })
            }
        });
    }).select('id_ name surname email moderator +password ')
}

module.exports = {
    register,
    login
}
