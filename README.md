# Chat API

API which manages the functionality of a Chat application on the client side

## Project setup
```
npm install
```

In this project directory, you can run:

### `npm start`

To start the Node server

### `npm run dev`

To execute  Nodemon for development mode

Http requests are received in the 3000 port and Socket.io actions in the 3001 port


