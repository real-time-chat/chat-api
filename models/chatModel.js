const mongoose = require('mongoose')
const Schema = mongoose.Schema;

/**
 * Define Chat model fields
 */
const chatSchema= new Schema({
    username: {type:String, required:true},
    text: {type:String, required:true},
    time: {type:String, required:true},
    moderator:{type:Boolean,required:true}
})

module.exports = mongoose.model('chat', chatSchema);
