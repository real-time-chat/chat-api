const mongoose = require('mongoose')
const Schema = mongoose.Schema;
const bcrypt_node=require('bcrypt-nodejs');

/**
 * Define User model fields
 */
const userSchema= new Schema({
    name: {type:String, required:true},
    surname: {type:String, required:true},
    email: {type:String, unique:true, lowercase:true, required:true},
    moderator:{type:Boolean, required:true},
    password: {type: String, select:false, required:true},
})

/**
*Executes before a user is stored as a MongoDB document
 */
userSchema.pre('save',function(next){
    if (!this.isModified('password')) return next()
    let user=this
    /**
     * Encrypts passwords who is received by the api, then is saved in a document
     */
    bcrypt_node.hash(user.password,null,null,(err,hash)=>{
          if(err) console.log(err)
          user.password =hash
      })
    next()
});

module.exports = mongoose.model('user', userSchema);
