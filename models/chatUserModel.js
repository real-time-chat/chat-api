const mongoose = require('mongoose')
const Schema = mongoose.Schema;

/**
 * Define Chat User model fields
 */
const chatUserSchema= new Schema({
    username: {type:String, required:true},
    room: {type:String, required:true},
    sessions:{type:Number,required:true},
    moderator:{type:Boolean,required:true}
})

module.exports = mongoose.model('chatUser', chatUserSchema);
