const express = require  ('express');
const router = express.Router();
const bodyParser = require('body-parser');
const path = require('path');
const xoAuth2= require('xoauth2')
const userController = require('../controllers/userController')
const isAuth=require('../middlewares/authentication')

// Define the api routes
router.post('/register', userController.register)
router.post('/login', userController.login)
router.get('/private',isAuth,(req,res)=>{
    res.status(200).send({message:'You have access'})
})

module.exports = router
