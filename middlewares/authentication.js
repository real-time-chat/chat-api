'use strict';

const services = require('../services');

/**
 * Check if an authentication token is valid
 * @param req Incoming token
 * @param res Server response
 * @param next Next middleware
 * @returns {*}
 */
function isAuth (req, res, next) {
    if (!req.headers.authorization) {
        return res.status(403).send({ message: 'You have no authorization' })
    }

    const token = req.headers.authorization.split(' ')[1]

    /**
     * Validates token
     */
    services.decodeToken(token)
        .then(response => {
            req.user = response
            next()
        })
        .catch(response => {
            return res.status(response.status).send({message:response.message})
        })
}

module.exports = isAuth
